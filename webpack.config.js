const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')

const webpackConfig = {
  target: 'node',
  context: __dirname,
  entry: {
    app: './src/app.js'
  },
  output: {
    path: __dirname,
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.sql$/,
        use: {
          loader: 'raw-loader'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.json', '.sql']
  },
  devtool: 'source-map',
  externals: [nodeExternals()],
  plugins: [
    new webpack.BannerPlugin({banner: '#!/usr/bin/env node', raw: true})
  ]
}

module.exports = webpackConfig
