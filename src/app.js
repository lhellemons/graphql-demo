import 'source-map-support/register'

import Koa from 'koa' // koa@2
import KoaRouter from 'koa-router' // koa-router@next
import koaBody from 'koa-bodyparser' // koa-bodyparser@next
import { graphqlKoa, graphiqlKoa } from 'apollo-server-koa'

import graphQLSchema from './schema'

const app = new Koa()
const router = new KoaRouter()

const GRAPHQL_PORT = 3000
const GRAPHQL_ENDPOINT = '/graphql'
const GRAPHIQL_ENDPOINT = '/graphiql'

// graphQL
router.post(GRAPHQL_ENDPOINT, koaBody(), graphqlKoa({schema: graphQLSchema}))
router.get(GRAPHQL_ENDPOINT, graphqlKoa({schema: graphQLSchema}))

// graphiQL
router.get(GRAPHIQL_ENDPOINT, graphiqlKoa({endpointURL: GRAPHQL_ENDPOINT}))


app.use(router.routes())
app.use(router.allowedMethods())
app.listen(GRAPHQL_PORT)

console.log(`Listening on localhost:${GRAPHQL_PORT}...`)