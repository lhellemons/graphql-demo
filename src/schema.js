import { makeExecutableSchema } from 'graphql-tools'
import getRandomTweetAbout from './twitter'

const typeDefs = `
  type Query {
    allSandwiches: [SandwichType!]
    sandwich(name: String!): SandwichType
    allIngredients: [IngredientType!]
  }
  
  type Mutation {
    orderSandwich(name: String!, quantity: Int): Float
    makeSandwich(name: String!, quantity: Int): SandwichType!
  }
  
  type SandwichType {
    name: String!
    price: Float!
    ingredients: [IngredientType!]!
    quantityLeft: Int!
    latestTweet: String!
  }
  
  type IngredientType {
    name: String!
    sandwiches: [SandwichType!]
  }
`

class SandwichType {
  constructor (name: string, price: number, ingredients: [string]) {
    this.name = name
    this.price = price
    this.ingredients = ingredients
    this.quantity = 0
  }

  make(quantity: number) {
    this.quantity += quantity
  }

  sell(quantity: number): number {
    if (quantity > this.quantity) {
      throw `Can't sell ${quantity} ${this.name}; only ${this.quantity} left!`
    }

    this.quantity -= quantity

    return quantity * this.price
  }
}

const mySandwiches = {}
mySandwiches.jongBelegenKaas = new SandwichType('jong belegen kaas', 3.00, ['kaas'])
mySandwiches.roombrie = new SandwichType('roombrie', 4.60, ['kaas'])
mySandwiches.toscaanseKip = new SandwichType('toscaanse kip', 4.50, ['kip'])

mySandwiches.jongBelegenKaas.make(10)
mySandwiches.roombrie.make(5)
mySandwiches.toscaanseKip.make(2)

const resolvers = {
  Query: {
    allSandwiches: () => Object.values(mySandwiches),
    sandwich: (_, { name }) => Object.values(mySandwiches).find(sandwich => sandwich.name === name),
    allIngredients: () => ['kaas', 'kip', 'tomaat']
  },

  SandwichType: {
    name: (sandwich: SandwichType) => sandwich.name,
    price: (sandwich: SandwichType) => sandwich.price,
    quantityLeft: (sandwich: SandwichType) => sandwich.quantity,
    ingredients: (sandwich: SandwichType) => sandwich.ingredients,
    latestTweet: async (sandwich: SandwichType) => await getRandomTweetAbout(sandwich.name)
  },

  IngredientType: {
    name: (ingredient: string) => ingredient,
    sandwiches: (ingredient: string) => Object.values(mySandwiches).filter(
      (sandwich: SandwichType) => sandwich.ingredients.includes(ingredient)
    )
  },


  Mutation: {
    orderSandwich: (_, { name, quantity}) => {
      const sandwichType = Object.values(mySandwiches).find(sandwich => sandwich.name === name)
      if (!sandwichType) {
        throw `Sandwich not found: ${name}`
      }
      return sandwichType.sell(quantity)
    },
    makeSandwich: (_, { name, quantity}) => {
      const sandwichType = Object.values(mySandwiches).find(sandwich => sandwich.name === name)
      if (!sandwichType) {
        throw `Sandwich not found: ${name}`
      }
      sandwichType.make(quantity)
      return sandwichType
    }
  }
}


const schema = makeExecutableSchema({
  typeDefs,
  resolvers
})

export default schema
